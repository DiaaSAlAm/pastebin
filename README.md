
# pastebin
A pastebin list of different row types, created as an iOS recruitment task for Ibtikar company.

Features
* Getting 3 sections. New Episodes, Channels, and Categories at the app launch
* Show specific row list
* Unit test

Requirements
* Xcode 12.3
* iOS 13+ 
* Swift 5+ 
 

Project Overview

Connecting to the pastebin API, the app shows a collection inside the tableView cell that matches the user's query. When tapping a row cell, the user is taken to a detail view controller, where they can find the specific row list:

* Storyboards/Nibs.
* Auto Layout.
* URLSession Network part. 
