//
//  PastebinTests.swift
//  PastebinTests
//
//  Created by Diaa SAlAm on 4/28/21.
//

import XCTest
@testable import Pastebin

class PastebinTests: XCTestCase {
    
    var presenter: HomePresenter?
    let mockInteractor = HomeInteractor()
    let mockRouter = MockRouter()
    let fakeNewEpisodesModel = [Media(type: "channel", title: "any", coverAsset: nil, channel: nil)] 
    let fakeSeriesModel = [Series(title: "Series", coverAsset: nil)]
    let fakeLatestMediaModel = [LatestMedia(type: "LatestMedia", title: "LatestMediaTitle", coverAsset: nil)]
    var fakeChannelsModel = [Channels]()
    var fakeCategoriesModel = [Categories(name: "We")]
    var channelsModel = [Channels]()
    var mockInterface = MockInterface()
     
    override func setUp() {
        presenter = HomePresenter(view: mockInterface, interactor: mockInteractor, router: mockRouter)
        mockInterface.presenter = presenter
        mockInteractor.presenter = presenter
        fakeChannelsModel = [Channels(title: "channel", series: fakeSeriesModel, mediaCount: 1, latestMedia: fakeLatestMediaModel, id: "1", iconAsset: nil, coverAsset: nil)]
    }
    
    func testRowsCountIs3() {
        presenter?.successRequest(model: fakeCategoriesModel)
        presenter?.successRequest(model: fakeChannelsModel)
        presenter?.successRequest(model: fakeNewEpisodesModel)
        XCTAssertEqual(presenter?.tableViewNumberOfRows, 3)
    }
    
    func testRowsAtIndexIsInjectedModel() {
        presenter?.successRequest(model: fakeCategoriesModel)
        let channel = presenter?.categoriesModel.getElement(at: 0)
        XCTAssertEqual(channel?.name, fakeCategoriesModel[0].name)
        
        presenter?.successRequest(model: fakeNewEpisodesModel)
        let newEpisodes = presenter?.newEpisodesModel.getElement(at: 0)
        XCTAssertEqual(newEpisodes?.title, fakeNewEpisodesModel[0].title)
    }
    
    func testRowsEmptyShouldShowError() {
        presenter?.failedRequest(withError: "")
        XCTAssertEqual(mockInterface.shouldHideLoadingIndicator,
                       true)
        XCTAssertEqual(mockInterface.shouldLoadList,
                       false)
    }
    
    func testDispatchGroupNotifyShouldShowData() {
        presenter?.successRequest(model: fakeNewEpisodesModel)
        presenter?.successRequest(model: fakeCategoriesModel)
        presenter?.successRequest(model: fakeChannelsModel)
        presenter?.dispatchGroup.notify(queue: .main) {
            XCTAssertEqual(self.mockInterface.shouldLoadList,
                           true)
            XCTAssertEqual(self.mockInterface.shouldHideLoadingIndicator,true)
        }
    }
    
    func testSelectedRows() {
        presenter?.successRequest(model: fakeNewEpisodesModel)
        presenter?.navigateRowsDetails(0)
        XCTAssertEqual(mockRouter.newEpisodesModel.getElement(at: 0)?.title, fakeNewEpisodesModel[0].title)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

class MockRouter: HomeRouterProtocol {
     var newEpisodesModel = [Media]()
     var seriesModel = [Series]()
     var latestMediaModel = [LatestMedia]()
      
    func openHomeDetails<M>(model: [M]) {
        if ((model as? [Media]) != nil) {
           newEpisodesModel = model as? [Media] ?? []
        }
        
        if ((model as? [Series]) != nil) {
          self.seriesModel = model as? [Series] ?? []
        }
        
        if ((model as? [LatestMedia]) != nil) {
          self.latestMediaModel = model as? [LatestMedia] ?? []
        }
    }
}


class MockInterface: HomeViewProtocol {
    var presenter: HomePresenterProtocol!
    var shouldLoadList = false
    var shouldShowLoadingIndicator = false
    var shouldHideLoadingIndicator = false
    
    func showLoadingIndicator() {
        shouldShowLoadingIndicator = true
    }
    
    func hideLoadingIndicator() {
        shouldHideLoadingIndicator = true
    }
    
    func successRequest() {
        shouldLoadList = true
    }
}


class MockTask: URLSessionDataTask {
    private let data: Data?
    private let urlResponse: URLResponse?

    private let _error: Error?
    override var error: Error? {
        return _error
    }

    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)!

    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        self.data = data
        self.urlResponse = urlResponse
        self._error = error
    }

    override func resume() {
        DispatchQueue.main.async {
            self.completionHandler(self.data, self.urlResponse, self.error)
        }
    }
}
