//
//  ExUIViewController.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 5/1/21.
//

 
import UIKit

extension UIViewController {
    
    // Hide the navigation bar on the this view controller
    func hideNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        navigationController?.navigationBar.isUserInteractionEnabled = false
    }
    // Show the navigation bar on other view controllers
    func showNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.isUserInteractionEnabled = true
    }
}
