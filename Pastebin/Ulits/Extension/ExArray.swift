//
//  ExArray.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
