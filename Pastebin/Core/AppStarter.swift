//
//  AppStarter.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    private let window = (SceneDelegate.shared?.window)
    
    private init() {}
    
    func start() {
        AppTheme.apply()
        setRootViewController()
    }
    
     
    
    private func setRootViewController() {
        let rootViewController = UINavigationController(rootViewController: HomeRouter.createModule())
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
}
