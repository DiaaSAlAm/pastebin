//
//  AppTheme.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

/// Apply application theme and colors
class AppTheme {
    
    static func apply() {
        navigationBarStyle() 
    }
    
    private static func navigationBarStyle() {
        UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0.1333333333, green: 0.1490196078, blue: 0.1803921569, alpha: 1)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        UINavigationBar.appearance().isTranslucent = false
    }
}
