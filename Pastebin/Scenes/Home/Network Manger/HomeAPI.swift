//
//  HomeAPI.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

protocol HomeAPIProtocol {
    
    func getNewEpisodes(completion: @escaping (Result<BaseResponse<NewEpisodesModel>?, Error>) -> Void)
    
    func getChannels(completion: @escaping (Result<BaseResponse<ChannelsModel>?, Error>) -> Void)
    
    func getCategories(completion: @escaping (Result<BaseResponse<CategoriesModel>?, Error>) -> Void)
}


class HomeAPI: BaseAPI<HomeNetwork>, HomeAPIProtocol {
    
    func getNewEpisodes(completion: @escaping (Result<BaseResponse<NewEpisodesModel>?, Error>) -> Void) {
        self.fetchData(target: .getNewEpisodes, responseClass: BaseResponse<NewEpisodesModel>.self, completion: completion)
    }
    
    func getChannels(completion: @escaping (Result<BaseResponse<ChannelsModel>?, Error>) -> Void) {
        self.fetchData(target: .getChannels, responseClass: BaseResponse<ChannelsModel>.self, completion: completion)
    }
    
    func getCategories(completion: @escaping (Result<BaseResponse<CategoriesModel>?, Error>) -> Void) {
        self.fetchData(target: .getCategories, responseClass: BaseResponse<CategoriesModel>.self, completion: completion)
    }
     
}
