//
//  HomeNetwork.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import Foundation

enum HomeNetwork {
    case getNewEpisodes
    case getChannels
    case getCategories
}

extension HomeNetwork: TargetType {
    
    var baseURL: String {
        return Environment.rootURL.absoluteString
    }
    
    var contentType: String {
        return "application/json"
    }
    
    
    var path: String {
        switch self {
        case .getNewEpisodes:
            return "z5AExTtw"
        case .getChannels:
            return "Xt12uVhM"
        case .getCategories:
            return "A0CgArX3"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            return [:]
        }
    }
    
    var task: Task {
        switch self { 
        default:
            return .requestPlain
        }
    }
}
