//
//  HomeRouter.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit
 
class HomeRouter: HomeRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = UIStoryboard(name: ConstantsStoryboardName.home.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(HomeVC.self)") as! HomeVC
        let interactor = HomeInteractor()
        let router = HomeRouter()
        let presenter = HomePresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func showMessage(message: String, messageKind: ToastMessageKind) {
        DispatchQueue.main.async {
            ToastManager.shared.showMessage(messageKind: messageKind, message: message)
        }
    }
    
    func openHomeDetails<M>(model: [M]){
        let view = HomeDetailsRouter.createModule(model: model)
        viewController?.navigationController?.pushViewController(view, animated: true)
    }

}
