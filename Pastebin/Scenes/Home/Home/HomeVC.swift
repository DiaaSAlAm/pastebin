//
//  HomeVC.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

class HomeVC: UIViewController, HomeViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    
    //MARK: - Properties
    var presenter: HomePresenterProtocol!
    private let cellIdentifier = ConstantsCellIdentifier.rowsCell.rawValue
    private let activityView = UIActivityIndicatorView(style: .large)
    private let fadeView: UIView = UIView()
    private var storedOffsets = [Int: CGFloat]()
    private let commonCellIdentifier = ConstantsCellIdentifier.commonCell.rawValue
    private let categoriesCell = ConstantsCellIdentifier.categoriesCell.rawValue
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showNavigationBar()
    }
    
    func showLoadingIndicator() {
        DispatchQueue.main.async {
            self.startAnimatingActivityView()
        }
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async {
            self.stopAnimatingActivityView()
        }
    }
    
    func successRequest() {
        tableView.reloadData()
    }
}

//MARK: Setup View
extension HomeVC {
    
    private func setupUI() {
        registerTableView()
        DispatchQueue.main.async {
            self.presenter.callRequests()
        }
    }
    
    private func registerTableView() {
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.register(UINib(nibName: categoriesCell, bundle: nil), forHeaderFooterViewReuseIdentifier: categoriesCell)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    //MARK: - Start Animating Activity
    private func startAnimatingActivityView() {
        fadeView.frame = tableView.frame
        fadeView.backgroundColor = .white
        fadeView.alpha = 0.6
        view.addSubview(fadeView)
        
        activityView.hidesWhenStopped = true
        activityView.center = view.center
        activityView.startAnimating()
        view.addSubview(activityView)
    }
    
    //MARK: - Stop Animating Activity
    private func stopAnimatingActivityView() {
        fadeView.removeFromSuperview()
        activityView.stopAnimating()
    }
}


// MARK: - UITableView HeaderInSection / FooterInSection
extension HomeVC {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presenter.newEpisodesNumberOfRows != 0 ? 400 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed(cellIdentifier, owner: self, options: nil)?.first as! RowsCell
        headerView.setCollectionViewDataSourceDelegate(self, forRow: 0)
        headerView.collectionViewOffset = storedOffsets[0] ?? 0
        presenter.rowsHeaderConfigureCell(cell: headerView)
        return presenter.newEpisodesNumberOfRows != 0 ? headerView : UIView()
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return presenter.categoriesNumberOfRow != 0 ? 500 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = Bundle.main.loadNibNamed(cellIdentifier, owner: self, options: nil)?.first as! RowsCell
        footerView.setCollectionViewDataSourceDelegate(self, forRow: 1)
        footerView.collectionViewOffset = storedOffsets[1] ?? 0
        presenter.rowsFooterConfigureCell(cell: footerView)
        return presenter.categoriesNumberOfRow != 0 ? footerView : UIView()
    }
    
}

// MARK: - UITableView Delegate
extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) }
        print("tableView Index ",indexPath.row)
        presenter.navigateRowsDetails(indexPath.row)
    }
}

// MARK: - UITableView Data Source
extension HomeVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.channelsNumberOfRow
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! RowsCell
        presenter.rowsConfigureCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? RowsCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row + 2) // + 2 for collectionView Tag for header and footer
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? RowsCell else { return }
        
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return presenter.newEpisodesNumberOfRows
        case 1:
            return presenter.categoriesNumberOfRow
        default:
            let tag = collectionView.tag
            return presenter.series_latestMediaNumberOfRow(tag)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let commonCell = collectionView.dequeueReusableCell(withReuseIdentifier: commonCellIdentifier,for: indexPath) as! CommonCell
        switch collectionView.tag {
        case 0: //row number 0 for footer New Episodes Cell
            presenter.newEpisodesConfigureCell(cell: commonCell, indexPath: indexPath)
            return commonCell
        case 1: //row number 1 for footer Categories Cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoriesCell,for: indexPath) as! CategoriesCell
            presenter.categoriesConfigureCell(cell: cell, indexPath: indexPath)
            return cell
        default:
            presenter.channelsConfigureCell(cell: commonCell, indexPath: indexPath, tag: collectionView.tag)
            return commonCell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        presenter.navigateRowsDetails(collectionView.tag)
    }
}


//MARK: - UICollectionView Delegate Flow Layout
extension HomeVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let tag = collectionView.tag 
        switch collectionView.tag {
        case 1: //row number 1 for footer Categories Cell
            return CGSize(width: (width / 2) - 8 , height: 80)
        default:
            return presenter.channelsDifferentDesignWidth(width: width, tag: tag)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
