//
//  Home.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

protocol HomeViewProtocol: class { //View Conteroller
    var presenter: HomePresenterProtocol! { get set }
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func successRequest()
}

protocol HomePresenterProtocol: class { // Logic
    var view: HomeViewProtocol? { get set }
    func callRequests()
    var newEpisodesModel: [Media] {get}
    var channelsModel: [Channels] {get}
    var categoriesModel: [Categories] {get}
    var newEpisodesNumberOfRows: Int {get}
    var categoriesNumberOfRow: Int {get}
    var channelsNumberOfRow: Int {get}
    func series_latestMediaNumberOfRow(_ tag: Int) -> Int
    func channelsDifferentDesignWidth(width: CGFloat, tag: Int) -> CGSize
    func newEpisodesConfigureCell(cell: CommonCellViewProtocol, indexPath: IndexPath)
    func channelsConfigureCell(cell: CommonCellViewProtocol, indexPath: IndexPath, tag collectionView: Int)
    func categoriesConfigureCell(cell: CategoriesCellViewProtocol, indexPath: IndexPath)
    func rowsConfigureCell(cell: RowsCellViewProtocol, indexPath: IndexPath)
    func rowsHeaderConfigureCell(cell: RowsCellViewProtocol)
    func rowsFooterConfigureCell(cell: RowsCellViewProtocol)
    func navigateRowsDetails(_ row: Int)
}

protocol HomeInteractorInputProtocol: class { // func do it from presenter
     var presenter: HomeInteractorOutputProtocol? { get set }
    func getNewEpisodes(completion: @escaping () -> Void)
    func getChannels(completion: @escaping () -> Void)
    func getCategories(completion: @escaping () -> Void)
}

protocol HomeInteractorOutputProtocol: class { // it's will call when interactor finished
    func successRequest<M>(model: M)
    func failedRequest(withError error: String)
    func showMessage(message: String)
}

protocol HomeRouterProtocol {  
    func showMessage(message: String, messageKind: ToastMessageKind)
    func openHomeDetails<M>(model: [M])
}

extension HomeRouterProtocol { // Make some protocol optional
    func showMessage(message: String, messageKind: ToastMessageKind) {
    }
}

protocol CommonCellViewProtocol { // it's will call when register cell
    func newEpisodesConfigureCell(viewModel: NewEpisodesViewModel)
    func channelsConfigureCell(viewModel: ChannelsViewModel)
     
}

protocol CategoriesCellViewProtocol { // it's will call when register cell
    func categoriesConfigureCell(viewModel: CategoriesViewModel)
     
}

protocol RowsCellViewProtocol { // it's will call when register cell
    func rowsConfigureCell(viewModel: RowsViewModel)
    func rowsHeaderConfigureCell(viewModel:RowsHeaderViewModel)
    func rowsFooterConfigureCell(viewModel:RowsFooterViewModel)
    
}
