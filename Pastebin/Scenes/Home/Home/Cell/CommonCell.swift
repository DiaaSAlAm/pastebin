//
//  CommonCell.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/30/21.
//

import UIKit

class CommonCell: UICollectionViewCell,CommonCellViewProtocol {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titelLb: UILabel!
    @IBOutlet weak var bodyLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // add corner radious
        img.layer.cornerRadius = 12
        img.layer.masksToBounds = true
    }
    
    func newEpisodesConfigureCell(viewModel: NewEpisodesViewModel) { 
        self.img.loadImageAsync(with: viewModel.urlImage)
        titelLb.text =  viewModel.title
        bodyLb.isHidden = false
        bodyLb.text = viewModel.title
    }
    
    func channelsConfigureCell(viewModel: ChannelsViewModel) {
        self.img.loadImageAsync(with: viewModel.urlImage)
        titelLb.text =  viewModel.title
        bodyLb.isHidden = true 
    }

}
