//
//  CategoriesCell.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/30/21.
//

import UIKit

class CategoriesCell: UICollectionViewCell, CategoriesCellViewProtocol {
   
    

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 30
    }
    
    func categoriesConfigureCell(viewModel: CategoriesViewModel) {
        labelText.text = viewModel.name
    }

}
