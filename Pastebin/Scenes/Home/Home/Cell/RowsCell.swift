//
//  RowsCell.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit
 
class RowsCell: UITableViewCell, RowsCellViewProtocol {

    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var channelsHeaderView: UIView!
    @IBOutlet weak var channelsHeaderLabel: UILabel!
    @IBOutlet weak var channelsHeaderImage: UIImageView!
    @IBOutlet weak var channelsHeaderNumberOfRows: UILabel!
    @IBOutlet weak var channelsLb: UILabel!
    
    //MARK: - Properties
    private let commonCell = ConstantsCellIdentifier.commonCell.rawValue
    private let categoriesCell = ConstantsCellIdentifier.categoriesCell.rawValue 
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
 
    func rowsConfigureCell(viewModel: RowsViewModel) {
        headerLabel.text = viewModel.headerLabel
        channelsLb.text = viewModel.headerLabel == "New Episodes" ? "Channels" : ""
        channelsHeaderView.isHidden = viewModel.channelsHeaderViewIsHidden
        channelsHeaderLabel.text = viewModel.channelsHeaderLabel
        channelsHeaderImage.loadImageAsync(with: viewModel.channelsHeaderImageURL)
        channelsHeaderNumberOfRows.text = viewModel.channelsHeaderNumberOfRows
    }
    
    func rowsHeaderConfigureCell(viewModel: RowsHeaderViewModel) {
        headerLabel.text = viewModel.headerLabel
        channelsLb.text = viewModel.headerLabel == "New Episodes" ? "Channels" : ""
        channelsHeaderView.isHidden = viewModel.channelsHeaderViewIsHidden
    }
    
    func rowsFooterConfigureCell(viewModel: RowsFooterViewModel) {
        headerLabel.text = viewModel.headerLabel
        channelsLb.text = viewModel.headerLabel == "New Episodes" ? "Channels" : ""
        channelsHeaderView.isHidden = viewModel.channelsHeaderViewIsHidden
    }
    
    //MARK: - set Collection View Data Source & Delegate for each
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        registerCollectionView()
        setCollectionViewLayout(row) // last row for Categories Cell change  scrollDirection
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
        
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.register(UINib(nibName: commonCell, bundle: nil), forCellWithReuseIdentifier: commonCell)
        collectionView.register(UINib(nibName: categoriesCell, bundle: nil), forCellWithReuseIdentifier: categoriesCell)
        
    }
    
    fileprivate func setCollectionViewLayout(_ row: Int){
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = row == 1 ? .vertical : .horizontal //  row number 1 for footer Categories Cell change  scrollDirection
        }
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
    
}

