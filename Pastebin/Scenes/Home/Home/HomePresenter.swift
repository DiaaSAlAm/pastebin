//
//  HomePresenter.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

class HomePresenter: HomePresenterProtocol, HomeInteractorOutputProtocol {
    
    weak var view: HomeViewProtocol?
    private let interactor: HomeInteractorInputProtocol
    private var router: HomeRouterProtocol
    private let queue1 = DispatchQueue(label: "queue 1")
    private let queue2 = DispatchQueue(label: "queue 2")
    private let queue3 = DispatchQueue(label: "queue 3")
    let dispatchGroup = DispatchGroup()
    var newEpisodesModel = [Media]()
    var channelsModel = [Channels]()
    var categoriesModel = [Categories]()
    var channelsNumberOfRow: Int {
        return channelsModel.count
    }
    var categoriesNumberOfRow: Int {
        return categoriesModel.count
    }
    //New Episodes and Channels should be scrollable with a maximum of 6 being shown per row.
    var newEpisodesNumberOfRows: Int {
        return newEpisodesModel.count > 6 ? 6 : newEpisodesModel.count
    } 
    
    init(view: HomeViewProtocol, interactor: HomeInteractorInputProtocol, router: HomeRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    //MARK: Call Request / Success / failed / show message
    func callRequests(){
        view?.showLoadingIndicator() 
        dispatchGroup.enter()
        queue1.async {
            self.interactor.getNewEpisodes {[weak self] in
                guard let self = self else {return}
                self.dispatchGroup.leave()
            }
        }

        dispatchGroup.enter()
        queue2.async {
            self.interactor.getChannels { [weak self] in
                guard let self = self else {return}
                self.dispatchGroup.leave()
            }
        }

        dispatchGroup.enter()
        queue3.async {
            self.interactor.getCategories {[weak self] in
                guard let self = self else {return}
                self.dispatchGroup.leave()
            }
        }
    }
    
    
    func successRequest<M>(model: M) {
        if ((model as? [Media]) != nil) {
          self.newEpisodesModel = model as? [Media] ?? []
        }
        
        if ((model as? [Channels]) != nil) {
          self.channelsModel = model as? [Channels] ?? []
        }
        
        if ((model as? [Categories]) != nil) {
          self.categoriesModel = model as? [Categories] ?? []
        }
        
        dispatchGroup.notify(queue: .main) {
            self.view?.hideLoadingIndicator()
            self.view?.successRequest()
        }
    }
    
    
    func failedRequest(withError error: String) {
        view?.hideLoadingIndicator()
        router.showMessage(message: error, messageKind: .error)
    }
    
    func showMessage(message: String) {
        router.showMessage(message: message, messageKind: .success)
    }
    
    //MARK: Configure Cell
    func rowsConfigureCell(cell: RowsCellViewProtocol, indexPath: IndexPath) {
        let data = channelsModel.getElement(at: indexPath.row)
        let viewModel = RowsViewModel(data: data)
        cell.rowsConfigureCell(viewModel: viewModel)
    }
    
    func rowsHeaderConfigureCell(cell: RowsCellViewProtocol) {
        let viewModel = RowsHeaderViewModel()
        cell.rowsHeaderConfigureCell(viewModel: viewModel)
    }
    
    func rowsFooterConfigureCell(cell: RowsCellViewProtocol) {
        let viewModel = RowsFooterViewModel()
        cell.rowsFooterConfigureCell(viewModel: viewModel)
    }
    
    func newEpisodesConfigureCell(cell: CommonCellViewProtocol, indexPath: IndexPath) {
        guard let data = newEpisodesModel.getElement(at: indexPath.row) else {return}
        let viewModel = NewEpisodesViewModel(data: data)
        cell.newEpisodesConfigureCell(viewModel: viewModel)
    }
    
    func channelsConfigureCell(cell: CommonCellViewProtocol, indexPath: IndexPath, tag: Int) {
        guard let data = channelsModel.getElement(at: tag - 2) else {return}
        let series = data.series?.getElement(at: indexPath.row)
        let latestMedia = data.latestMedia?.getElement(at: indexPath.row)
        let viewModel = ChannelsViewModel(series: series, latestMedia: latestMedia)
        cell.channelsConfigureCell(viewModel: viewModel)
    }
    
    func categoriesConfigureCell(cell: CategoriesCellViewProtocol, indexPath: IndexPath) {
        guard let data = categoriesModel.getElement(at: indexPath.row) else {return}
        let viewModel = CategoriesViewModel(data: data)
        cell.categoriesConfigureCell(viewModel: viewModel)
    }
    
    //MARK: return number of row depending on object type
    private func seriesNumberOfRow(_ tag: Int) -> Int {
        let channels = channelsModel.getElement(at: tag - 2 ) //- 2 from collection view tag
        let seriesCount =  channels?.series?.count ?? 0
        let count = seriesCount > 6 ? 6 : seriesCount //New Episodes and Channels should be scrollable with a maximum of 6 being shown per row.
        return count
    }
    
    private func latestMediaNumberOfRow(_ tag: Int) -> Int {
        let channels = channelsModel.getElement(at: tag - 2 ) //- 2 from collection view tag
        let latestMediaCount =  channels?.latestMedia?.count ?? 0
        let count = latestMediaCount > 6 ? 6 : latestMediaCount //New Episodes and Channels should be scrollable with a maximum of 6 being shown per row.
        return count
    }
    
    func series_latestMediaNumberOfRow(_ tag: Int) -> Int {
        let count = seriesNumberOfRow(tag) != 0 ? seriesNumberOfRow(tag) : latestMediaNumberOfRow(tag)
        return count
    }
    
    //MARK: return CGSize of row depending on object type
    func channelsDifferentDesignWidth(width: CGFloat, tag: Int) -> CGSize{
        let seriesCount = seriesNumberOfRow(tag - 2) //- 2 from collection view tag
        let seriesWidth = width / 1.2
        let courseWidth = width / 2
        let channelsWidth = seriesCount != 0 ?  seriesWidth : courseWidth
        return CGSize(width: channelsWidth , height: 300)
    }
    
    //MARK: Navigate to Rows Details
    func navigateRowsDetails(_ row: Int){
        if row == 0 { 
            navigateModel(model: newEpisodesModel)
        }else if row  == 1{
            return // Void navigate when tapped on categories cell
        }else{
            guard let data = channelsModel.getElement(at: row - 2) else {return}
            if let series = data.series, series.count != 0 {
                navigateModel(model: series)
                return
            }
            if let latestMedia = data.latestMedia, latestMedia.count != 0  {
                navigateModel(model: latestMedia)
                return
            }
        }
    }
    
    func navigateModel<M>(model: [M]) {
        router.openHomeDetails(model: model)
    }
}
