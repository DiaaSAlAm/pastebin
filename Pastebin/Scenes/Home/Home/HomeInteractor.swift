//
//  HomeInteractor.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

class HomeInteractor: HomeInteractorInputProtocol {

    weak var presenter: HomeInteractorOutputProtocol?
    private let homeNetwork: HomeAPIProtocol = HomeAPI()
    
    
    func getNewEpisodes(completion: @escaping () -> Void){
        homeNetwork.getNewEpisodes { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                guard let model = response?.data?.media , model.count != 0 else {
                    self.presenter?.failedRequest(withError:  ConstantsMessages.noDataLoaded.rawValue)
                    return
                }
                self.presenter?.successRequest(model: model)
            case .failure(let error):
                let err = error.localizedDescription
                print(err)
                self.presenter?.failedRequest(withError: err)
            }
        }
        completion()
    }
    
    func getChannels(completion: @escaping () -> Void){
        homeNetwork.getChannels { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                guard let model = response?.data?.channels , model.count != 0 else {
                    self.presenter?.failedRequest(withError:  ConstantsMessages.noDataLoaded.rawValue)
                    return
                }
                self.presenter?.successRequest(model: model)
            case .failure(let error):
                let err = error.localizedDescription
                print(err)
                self.presenter?.failedRequest(withError: err)
            }
        }
        completion()
    }
    
    func getCategories(completion: @escaping () -> Void){
        homeNetwork.getCategories { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                guard let model = response?.data?.categories , model.count != 0 else {
                    self.presenter?.failedRequest(withError:  ConstantsMessages.noDataLoaded.rawValue)
                    return
                }
                self.presenter?.successRequest(model: model)
            case .failure(let error):
                let err = error.localizedDescription
                print(err)
                self.presenter?.failedRequest(withError: err)  
            }
        }
        completion()
    }
     
}
