//
//  CategoriesModel.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/30/21.
//

import Foundation

struct CategoriesModel : Codable {
    let categories : [Categories]? 
}

struct Categories : Codable {
    let name : String?
}

struct CategoriesViewModel {
    var name: String?
    
    init(data: Categories) {
        self.name = data.name
    }
}
