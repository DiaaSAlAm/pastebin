//
//  NewEpisodesModel.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/30/21.
//

import Foundation

struct NewEpisodesModel : Codable {
    let media : [Media]?
}

struct Media : Codable {
    let type : String?
    let title : String?
    let coverAsset : CoverAsset?
    let channel : Channel?
}

struct Channel : Codable {
    let title : String?
}


struct CoverAsset : Codable {
    let url : String?
}

struct NewEpisodesViewModel {
    var urlImage: String?
    var title : String?
    var body: String? 
    
    init(data: Media) {
        self.urlImage = data.coverAsset?.url
        self.title = data.title
        self.body = data.channel?.title
    }
}
