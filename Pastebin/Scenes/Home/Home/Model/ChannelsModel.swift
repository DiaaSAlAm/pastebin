//
//  ChannelsModel.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/30/21.
//

import Foundation

struct ChannelsModel : Codable {
    let channels : [Channels]?
}

struct Channels : Codable {
    let title : String?
    let series : [Series]?
    let mediaCount : Int?
    let latestMedia : [LatestMedia]?
    let id : String?
    let iconAsset : IconAsset?
    let coverAsset : CoverAsset?
}

struct LatestMedia : Codable {
    let type : String?
    let title : String?
    let coverAsset : CoverAsset?
}


struct IconAsset : Codable {
    let thumbnailUrl : String?
    let url: String?

}

struct Series : Codable {
    let title : String?
    let coverAsset : CoverAsset?  
}

struct ChannelsViewModel {
    var urlImage: String
    var title : String
    
    init(series: Series?, latestMedia: LatestMedia?) {
        //check design type (Course - Series ) series != nil -> Series : Course
        self.urlImage = series?.coverAsset?.url ??  latestMedia?.coverAsset?.url ?? ""
        self.title = series?.title ??  latestMedia?.title ?? ""
    }
}
 
