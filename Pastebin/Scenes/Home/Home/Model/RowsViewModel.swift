//
//  RowsViewModel.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 5/1/21.
//

import Foundation

struct RowsViewModel {
    var headerLabel: String = ""
    var channelsHeaderLabel: String?
    var channelsHeaderImageURL: String?
    var channelsHeaderNumberOfRows: String?
    var channelsHeaderViewIsHidden: Bool = false
    
    init(data: Channels?) { 
        channelsHeaderLabel = data?.title
        let iconAssetUrl = data?.iconAsset?.url != nil ? data?.iconAsset?.url  : data?.iconAsset?.thumbnailUrl //check design type (Course - Series ) Url != nil -> Series : Course
        channelsHeaderImageURL = iconAssetUrl
        let numberOfRows = data?.series?.count != 0 ? "\(data?.series?.count ?? 0) series" : "\(data?.latestMedia?.count ?? 0) episodes"
        channelsHeaderNumberOfRows = numberOfRows
        
    }
}


struct RowsHeaderViewModel {
    var headerLabel = ConstantsString.newEpisodes.rawValue
    var channelsHeaderViewIsHidden: Bool = true
}


struct RowsFooterViewModel {
    var headerLabel = ConstantsString.browesByCategories.rawValue
    var channelsHeaderViewIsHidden: Bool = true
}
