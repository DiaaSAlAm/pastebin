//
//  HomeDetailsRouter.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 5/1/21.
//

import UIKit
 
class HomeDetailsRouter: HomeDetailsRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    static func createModule<M>(model: [M]) -> UIViewController {
        let view = UIStoryboard(name: ConstantsStoryboardName.home.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(HomeDetailsVC.self)") as! HomeDetailsVC
        let interactor = HomeDetailsInteractor()
        let router = HomeDetailsRouter() 
        let presenter = HomeDetailsPresenter(view: view, interactor: interactor, router: router, model: model)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }

}
