//
//  HomeDetailsPresenter.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 5/1/21.
//

import UIKit

class HomeDetailsPresenter<M>: HomeDetailsPresenterProtocol, HomeDetailsInteractorOutputProtocol {
    
    weak var view: HomeDetailsViewProtocol?
    private let interactor: HomeDetailsInteractorInputProtocol
    private var router: HomeDetailsRouterProtocol
    private var newEpisodesModel = [Media]()
    private var seriesModel = [Series]()
    private var latestMediaModel = [LatestMedia]()
    var numberOfRow: Int = 0
    
    init(view: HomeDetailsViewProtocol, interactor: HomeDetailsInteractorInputProtocol, router: HomeDetailsRouterProtocol, model: [M]?) {
        self.view = view
        self.interactor = interactor
        self.router = router
        if ((model as? [Media]) != nil) {
           newEpisodesModel = model as? [Media] ?? []
            numberOfRow = newEpisodesModel.count
        }
        
        if ((model as? [Series]) != nil) {
          self.seriesModel = model as? [Series] ?? []
            numberOfRow = seriesModel.count
        }
        
        if ((model as? [LatestMedia]) != nil) {
          self.latestMediaModel = model as? [LatestMedia] ?? []
            numberOfRow = latestMediaModel.count
        }
    }
     
    
    
    func configureCell(cell: CommonCellViewProtocol, indexPath: IndexPath){
        if newEpisodesModel.count != 0 {
            guard let data = newEpisodesModel.getElement(at: indexPath.row) else {return}
            let viewModel = NewEpisodesViewModel(data: data)
            cell.newEpisodesConfigureCell(viewModel: viewModel)
        }else{
            let series = seriesModel.getElement(at: indexPath.row)
            let latestMedia = latestMediaModel.getElement(at: indexPath.row)
            let viewModel = ChannelsViewModel(series: series, latestMedia: latestMedia)
            cell.channelsConfigureCell(viewModel: viewModel)
        }
    }
     
}
