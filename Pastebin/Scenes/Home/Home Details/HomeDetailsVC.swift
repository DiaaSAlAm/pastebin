//
//  HomeDetailsVC.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 5/1/21.
//

import UIKit

class HomeDetailsVC: UIViewController, HomeDetailsViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var presenter: HomeDetailsPresenterProtocol!
    private let cellIdentifier = ConstantsCellIdentifier.commonCell.rawValue
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionView()
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        collectionView.reloadData()
    }
}
 


//MARK: - UICollectionView Data Source
extension HomeDetailsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRow
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,for: indexPath) as! CommonCell
        presenter.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension HomeDetailsVC: UICollectionViewDelegateFlowLayout { 
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width  - 8 , height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
