//
//  HomeDetails.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 5/1/21.
//

import UIKit

protocol HomeDetailsViewProtocol: class { //View Conteroller
    var presenter: HomeDetailsPresenterProtocol! { get set }
}

protocol HomeDetailsPresenterProtocol: class { // Logic
    var view: HomeDetailsViewProtocol? { get set }
    func configureCell(cell: CommonCellViewProtocol, indexPath: IndexPath)
    var numberOfRow: Int {get}
}

protocol HomeDetailsInteractorInputProtocol: class { // func do it from presenter
     var presenter: HomeDetailsInteractorOutputProtocol? { get set }
    
}

protocol HomeDetailsInteractorOutputProtocol: class { // it's will call when interactor finished
    
}

protocol HomeDetailsRouterProtocol { 
     
}
