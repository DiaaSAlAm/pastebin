//
//  ConstantsString.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import Foundation

enum ConstantsString: String{
    case newEpisodes = "New Episodes"
    case browesByCategories = "Browse by categories"
}
