//
//  ConstantsMessages.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import Foundation

enum ConstantsMessages: String{
    case genericErrorMessage = "Something went wrong please try again later"
    case noDataLoaded = "No Data Loaded"
}
