//
//  ConstantsCellIdentifier.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import Foundation

enum ConstantsCellIdentifier: String{
    case rowsCell = "RowsCell"
    case commonCell = "CommonCell"
    case categoriesCell = "CategoriesCell"
}
