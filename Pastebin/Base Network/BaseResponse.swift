//
//  BaseResponse.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import Foundation

class BaseResponse<T: Codable>: Codable {
    var data: T?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}
