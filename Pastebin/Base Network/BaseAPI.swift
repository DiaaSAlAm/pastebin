//
//  BaseAPI.swift
//  Pastebin
//
//  Created by Diaa SAlAm on 4/28/21.
//

import UIKit

class BaseAPI<T: TargetType> {
    
    private let allowedDiskSize = 100 * 1024 * 1024
    var cacheImageKey: NSString = ""
    let cacheImage = NSCache<NSString, UIImage>()
    private lazy var cache: URLCache = {
        return URLCache(memoryCapacity: 0, diskCapacity: allowedDiskSize, diskPath: "gifCache")
    }()
    
    private func createAndRetrieveURLSession() -> URLSession {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.requestCachePolicy = .returnCacheDataElseLoad
        sessionConfiguration.urlCache = cache
        return URLSession(configuration: sessionConfiguration)
    }
    
    func fetchData<M: Decodable>(target: T, responseClass: M.Type, completion:@escaping (Result<M?, Error>) -> Void) {
        
        let httpMethod =  target.method.rawValue
        let parameters =  buildParams(task: target.task).0
        let httpBody = buildParams(task: target.task).1
        let headers =  target.headers
        let urlString = target.baseURL + target.path
        let url = buildURL(task: target.task, parameters: parameters ?? [:], urlString: urlString)
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.httpBody = httpBody
        request.allHTTPHeaderFields = headers
        
        let dataTask = createAndRetrieveURLSession().dataTask(with: request) { [weak self] (data, response,error) in
            guard let self = self else {return}
            if error != nil  {
                self.loadFromCashData(target: target, responseClass: responseClass, request: request, error: error!, completion: completion) 
                return
            }
            
            do {
                let responseObj = try JSONDecoder().decode(M.self, from: data!)
                completion(.success(responseObj))
                return
            }catch let error {
                print(error.localizedDescription)
                completion(.failure(error))
                return
            }
        }
        dataTask.resume()
    }
    
    func loadFromCashData<M: Decodable>(target: T, responseClass: M.Type,request: URLRequest, error: Error, completion:@escaping (Result<M?, Error>) -> Void){
        if let cachedData = self.cache.cachedResponse(for: request) {
            print("Cached data in bytes:", cachedData.data)
            do {
                let responseObj = try JSONDecoder().decode(M.self, from: cachedData.data)
                completion(.success(responseObj))
                return
            }catch let error {
                print(error)
                completion(.failure(error))
                return
            } 
        }
        completion(.failure(error))
    }
     
    
    private func buildParams(task: Task) -> ([String: Any]?, Data?) {
        switch task {
        case .requestPlain:
            return (nil,nil)
        case .requestParametersJSONEncoding(let parameters):
            return (nil, try? JSONSerialization.data(withJSONObject: parameters))
        case .requestParametersURLEncoding(let parameters):
            return (parameters, nil)
        }
    }
    
    private func buildURL(task: Task,parameters: [String: Any], urlString: String ) -> URL {
        switch task {
        case .requestParametersURLEncoding:
            var urlComponents = URLComponents(string: urlString)
            var queryItems = [URLQueryItem]()
            for (key, value) in parameters {
                queryItems.append(URLQueryItem(name: key, value: "\(value)"))
            }
            urlComponents?.queryItems = queryItems
            return (urlComponents?.url!)!
        default:
            return URL(string: urlString)!
        }
    }
}
